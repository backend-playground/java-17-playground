package org.example.service;

import org.example.model.TestA;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class OptionalNullable {

    List<TestA> listA = Arrays.asList(new TestA("aa",1),new TestA("bb",2));
//    List<TestA> listA = null;
}
