package org.example.service;

import org.example.model.TestA;

import java.util.stream.Stream;

public class StreamPeek {

    public void testPeek() {
        TestA testA1 = new TestA();
        testA1.setA("aa");
        testA1.setB(11);

        TestA testA2 = new TestA();
        testA2.setA("bb");
        testA2.setB(22);

        var result = Stream.of(testA1, testA2)
                .peek(x -> {
                    x.setA("cc");
                    x.setB(33);
                })
                .toList();

        result.forEach(x->System.out.println("a:"+x.getA()+",  b:"+x.getB()));
    }
}
