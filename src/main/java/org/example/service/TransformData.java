package org.example.service;

import java.util.*;
        import java.util.stream.Collectors;
import java.util.stream.Stream;

class Data {
    int a;
    String b;

    public Data(int a, String b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public String getB() {
        return b;
    }

    @Override
    public String toString() {
        return "{a:" + a + ", b:" + b + "}";
    }
}

class TransformedData {
    int a;
    List<String> b;

    public TransformedData(int a, List<String> b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String toString() {
        return "{a:" + a + ", b:" + b + "}";
    }
}

public class TransformData {
    public static void main(String[] args) {
        List<Data> dataList = Arrays.asList(
                new Data(0, "kk"),
                new Data(0, "rr"),
                new Data(1, "uu"),
                new Data(1, "aa")
        );

        Map<Integer, List<String>> groupedData = dataList.stream()
                .collect(Collectors.groupingBy(
                        Data::getA,
                        Collectors.mapping(Data::getB, Collectors.toList())
                ));

        List<TransformedData> transformedDataList = groupedData.entrySet().stream()
                .peek(System.out::println)
                .map(entry -> new TransformedData(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        transformedDataList.forEach(System.out::println);
    }
}



class MerchantIdProcessor {

    public static void main(String[] args) {
        // Example usage
        PortalJobAmendBranchNo portalJobAmendBranchNo = new PortalJobAmendBranchNo();
        portalJobAmendBranchNo.setMerchantId("123,456,789");

        var merchantIdList = Stream.of(portalJobAmendBranchNo.getMerchantId().split(","))
                .map(MerchantId::new)
                .toList();
        merchantIdList.forEach(x->System.out.println("merchantId: " + x.getMerchantId()));


        // Now amendSfRequest contains the processed merchant IDs
    }
}

class PortalJobAmendBranchNo {
    private String merchantId;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}

class MerchantId {
    public MerchantId(String merchantId){
        this.merchantId = merchantId;
    }
    private String merchantId;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}

class AmendSfRequest {
    private List<MerchantId> merchantId;

    public List<MerchantId> getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(List<MerchantId> merchantId) {
        this.merchantId = merchantId;
    }
}
